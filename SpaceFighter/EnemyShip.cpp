
#include "EnemyShip.h"


EnemyShip::EnemyShip()
{
	// sets the hitpoints of the enemy ships to 1
	SetMaxHitPoints(1);
	// gives each enemy ship a radius of 20 pixels around it where it can be hit 
	SetCollisionRadius(20);
}


void EnemyShip::Update(const GameTime *pGameTime)
{
	if (m_delaySeconds > 0)
	{
		m_delaySeconds -= pGameTime->GetTimeElapsed();

		if (m_delaySeconds <= 0)
		{
			GameObject::Activate();
		}
	}

	if (IsActive())
	{
		m_activationSeconds += pGameTime->GetTimeElapsed();
		if (m_activationSeconds > 2 && !IsOnScreen()) Deactivate();
	}

	Ship::Update(pGameTime);
}


void EnemyShip::Initialize(const Vector2 position, const double delaySeconds)
{
	// Gives a position to the enemy ship
	SetPosition(position);
	// sets the delay before the ship appears on screen
	m_delaySeconds = delaySeconds;
	
	Ship::Initialize();
}


void EnemyShip::Hit(const float damage)
{
	Ship::Hit(damage);
}